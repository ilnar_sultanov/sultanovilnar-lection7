package com;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

public class PluginManager {
    private final String pluginRootDirectory;

    public PluginManager(String pluginRootDirectory) {
        this.pluginRootDirectory = pluginRootDirectory;
    }


    public List<Plugin> load(String pluginName, String pluginClassName)
            throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException {

        File pluginDir = new File(pluginRootDirectory);
        File[] jars = pluginDir.listFiles(file -> file.isFile() && file.getName().endsWith(pluginName + ".jar"));
        List<Plugin> pluginClasses = null;

        if (jars != null) {
            pluginClasses = new ArrayList<>(jars.length);

            for (File jar : jars) {
                URL jarURL = jar.toURI().toURL();
                URLClassLoader classLoader = new URLClassLoader(new URL[]{jarURL});
                pluginClasses.add((Plugin) classLoader.loadClass("com." + pluginClassName).newInstance());
            }
        }

        return pluginClasses;
    }
}
