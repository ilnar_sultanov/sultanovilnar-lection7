package com;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Browser {

    private static final String pluginRootDirectory = "src/main/resources/pluginRootDirectory/pluginName";

    public static void main(String[] args) {
        PluginManager pluginManager = new PluginManager(pluginRootDirectory);

        List<String> pluginsFromRootDirectory = new ArrayList<>();
        pluginsFromRootDirectory.add("FirstPlugin");
        pluginsFromRootDirectory.add("SecondPlugin");

        List<Plugin> allPlugins = new ArrayList<>();

        for (String pluginName : pluginsFromRootDirectory) {
            try {
                allPlugins.addAll(pluginManager.load(pluginName, "PluginImpl"));
            } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | IOException e) {
                e.printStackTrace();
            }
        }

        for (Plugin plugin: allPlugins){
            plugin.doUseful();
        }
    }
}
